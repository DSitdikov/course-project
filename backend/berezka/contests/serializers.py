from rest_framework import serializers
from .models import User, Bot, Contest, GameResult, MatchResult


class ShortUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username')


class ShortContestSerializer(serializers.ModelSerializer):
    participants = serializers.SerializerMethodField()

    class Meta:
        model = Contest
        fields = ('id', 'title', 'participants', 'beginning', 'description')

    def get_participants(self, obj):
        return obj.participants.count()


class BotSerializerBase(serializers.ModelSerializer):
    owner = ShortUserSerializer(read_only=True)
    race = serializers.SerializerMethodField()
    source = serializers.SerializerMethodField()

    def get_race(self, obj):
        return {
            'P': 'Protoss',
            'T': 'Terran',
            'Z': 'Zerg',
            'R': 'Random',
        }.get(obj.race)

    def get_source(self, obj):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        if user is not None and obj.owner == user:
            return obj.source_code.url
        return ''


class ShortBotSerializer(BotSerializerBase):
    class Meta:
        model = Bot
        fields = ('id', 'owner', 'name', 'race')


class BotSerializer(BotSerializerBase):
    class Meta:
        model = Bot
        fields = ('id', 'owner', 'name', 'race', 'created', 'modified',
                  'source')


class GameResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = GameResult
        fields = ('id', 'map_name', 'date', 'result')


class ShortMatchResultSerializer(serializers.ModelSerializer):
    player1 = ShortBotSerializer(read_only=True)
    player2 = ShortBotSerializer(read_only=True)

    class Meta:
        model = MatchResult
        fields = ('id', 'player1', 'player2', 'score1', 'score2')


class MatchResultSerializer(serializers.ModelSerializer):
    player1 = ShortBotSerializer(read_only=True)
    player2 = ShortBotSerializer(read_only=True)
    results = GameResultSerializer(read_only=True, many=True)

    class Meta:
        model = MatchResult
        fields = ('id', 'player1', 'player2', 'score1', 'score2', 'results')


class ContestSerializer(serializers.ModelSerializer):
    participants = serializers.SerializerMethodField()
    results = MatchResultSerializer(read_only=True, many=True)

    class Meta:
        model = Contest
        fields = ('id', 'title', 'participants', 'beginning', 'description',
                  'results')

    def get_participants(self, obj):
        return obj.participants.count()
