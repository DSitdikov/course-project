from django.utils import timezone
from rest_framework import generics, permissions

from .models import User, Bot, MatchResult, Contest
from .permissions import IsBotOwnerOrReadOnly
from .serializers import (ShortUserSerializer, BotSerializer,
                          ContestSerializer, MatchResultSerializer,
                          ShortContestSerializer)


class UserMixin:
    model = User
    queryset = User.objects.all()
    serializer_class = ShortUserSerializer


class UserList(UserMixin, generics.ListAPIView):
    pass


class UserDetail(UserMixin, generics.RetrieveAPIView):
    # lookup_field = 'username'
    pass


class BotMixin:
    model = Bot
    queryset = Bot.objects.all()
    serializer_class = BotSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,
                          IsBotOwnerOrReadOnly)

    def perform_create(self, serializer):
        """Force owner to the current user on save"""
        serializer.save(owner=self.request.user)


class BotList(BotMixin, generics.ListAPIView):
    pass


class UserBotList(BotMixin, generics.ListAPIView):
    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(owner__pk=self.kwargs.get('owner_pk'))


class BotDetail(BotMixin, generics.RetrieveUpdateAPIView):
    pass


class UpcomingContestMixin:
    model = Contest
    queryset = Contest.objects.filter(beginning__gte=timezone.now())
    serializer_class = ShortContestSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class UpcomingContestList(UpcomingContestMixin, generics.ListAPIView):
    pass


class PastContestMixin:
    model = Contest
    queryset = Contest.objects.filter(beginning__lt=timezone.now())
    serializer_class = ShortContestSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class PastContestList(PastContestMixin, generics.ListCreateAPIView):
    pass


class ContestMixin:
    model = Contest
    queryset = Contest.objects.all()
    serializer_class = ContestSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class ContestDetail(ContestMixin, generics.RetrieveAPIView):
    pass


class MatchResultMixin:
    model = MatchResult
    queryset = MatchResult.objects.all()
    serializer_class = MatchResultSerializer


class MatchResultDetail(MatchResultMixin, generics.RetrieveAPIView):
    pass
