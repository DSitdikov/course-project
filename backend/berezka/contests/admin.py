from django.contrib import admin
from .models import Bot, Contest, MatchResult, GameResult


admin.site.register(Bot)
admin.site.register(Contest)
admin.site.register(GameResult)
admin.site.register(MatchResult)
