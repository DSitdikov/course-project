from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone


class Bot(models.Model):
    PROTOSS = 'P'
    TERRAN = 'T'
    ZERG = 'Z'
    RANDOM = 'R'

    RACES = (
        (PROTOSS, 'Protoss'),
        (TERRAN, 'Terran'),
        (ZERG, 'Zerg'),
        (RANDOM, 'Random'),
    )

    owner = models.ForeignKey(User)
    is_active = models.BooleanField(default=True)
    name = models.CharField(max_length=255, unique=True)
    race = models.CharField(max_length=1, choices=RACES)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    source_code = models.FileField()

    def __str__(self):
        return '{} [{}]'.format(self.name, self.race)


class Contest(models.Model):
    title = models.CharField(max_length=255)
    # TODO: TextField -> HTMLField
    description = models.TextField()
    beginning = models.DateTimeField()
    participants = models.ManyToManyField(Bot, related_name='contests',
                                          blank=True)

    def __str__(self):
        return self.title

    @property
    def is_over(self):
        return self.beginning < timezone.now()

    @property
    def is_active(self):
        return not self.is_over


class MatchResult(models.Model):
    contest = models.ForeignKey(Contest, related_name='results')
    player1 = models.ForeignKey(Bot, related_name='player1_results')
    player2 = models.ForeignKey(Bot, related_name='player2_results')
    score1 = models.IntegerField(default=0)
    score2 = models.IntegerField(default=0)

    def __str__(self):
        return '{}. {} — {}'.format(
            self.contest.title,
            self.player1.owner.username,
            self.player2.owner.username
        )

    @property
    def winner(self):
        return self.player1 if self.score1 > self.score2 else self.player2


class GameResult(models.Model):
    DRAW = 0
    PLAYER1 = 1
    PLAYER2 = 2

    RESULTS = (
        (DRAW, 'Draw'),
        (PLAYER1, 'Player #1'),
        (PLAYER2, 'Player #2'),
    )

    match = models.ForeignKey(MatchResult, related_name='results')
    result = models.IntegerField(default=0, choices=RESULTS)
    map_name = models.CharField(max_length=255)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{} ({})'.format(str(self.match), self.map_name)
