from django.conf.urls import url, include
from contests.api import (BotList, BotDetail, UpcomingContestList,
                          PastContestList, ContestDetail, UserBotList,
                          MatchResult)


user_urls = [
    url(r'^(?P<owner_pk>\d+)/bots/$', UserBotList.as_view(), name='bot-list'),
]

bot_urls = [
    url(r'^$', BotList.as_view(), name='bot-list'),
    url(r'^(?P<pk>\d+)/$', BotDetail.as_view(), name='bot-detail'),
]

contest_urls = [
    url(r'^upcoming/$', UpcomingContestList.as_view(),
        name='upcoming-contests'),
    url(r'^past/$', PastContestList.as_view(), name='past-contests'),
    url(r'^(?P<pk>\d+)/$', ContestDetail.as_view(),
        name='contest-detail'),
]

urlpatterns = [
    url(r'^bots/', include(bot_urls)),
    url(r'^contests/', include(contest_urls)),
    url(r'^users/', include(user_urls)),
]
