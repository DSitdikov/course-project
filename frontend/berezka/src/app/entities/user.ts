export class User {
    pk: number;
    username: string;
    email: string;
    password: string;
    first_name: string;
    last_name: string;

    constructor(data?: any) {
        data = data || {};

        this.pk = +data.pk || -1;
        this.username = data.username || '';
        this.email = data.email || '';
        this.password = data.password || '';
        this.first_name = data.first_name || '';
        this.last_name = data.last_name || '';
    }
}
