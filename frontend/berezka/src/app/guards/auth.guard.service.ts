import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    private logged: boolean;

    constructor(private router: Router, private authService: AuthService) {
    }

    canActivate() {
        const logged = this.authService.isLogged();

        console.log('Auth Guard');
        console.log(logged);

        if (logged) {
            return true;
        }

        this.router.navigate(['login/']);
        return false;
    }
}
