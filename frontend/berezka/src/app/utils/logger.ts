import { LoggingLevelType } from './logger-level-type.enum';
import { environment as env } from '../../environments/environment';

/**
 * Logger wraps console methods.
 * It adds such bonuses like:
 *      - base prefix, for instance: [bp] ...
 *      - log level, for instance: DEBUG
 *      - prefix, for instance: ... [wa-client] ...
 *
 * author: akalashnikov
 * date: 21.04.2017.
 */
export class Logger {
    basePrefix: string;
    logLevel: LoggingLevelType;
    prefix: string;

    constructor(options: any) {
        options = options || {};

        this.basePrefix = options.basePrefix || 'bp';
        this.logLevel = options.logLevel || (env.logLevel ? LoggingLevelType[env.logLevel] : LoggingLevelType.ERROR);
        this.prefix = options.prefix || '';
    }

    private invoke(functionName: string, logLevel: LoggingLevelType, ...args: any[]): void {
        if (logLevel <= this.logLevel) {
            const basePrefix = this.basePrefix ? '[' + this.basePrefix + ']' : '';
            const prefix = this.prefix ? '[' + this.prefix + ']' : '';

            if (typeof args[0] === 'string') {
                args[0] = basePrefix + prefix + ' ' + args[0];
            } else {
                Array.prototype.splice.call(args, 0, 0, basePrefix + prefix);
            }

            console[functionName].apply(console, args);
        }
    }

    /**
     * Method wraps console.log() function.
     *
     * @param args Various array of arguments to log.
     */
    log(...args: any[]): void {
        this.invoke('log', LoggingLevelType.DEBUG, ...args);
    }

    /**
     * Method wraps console.info() function.
     *
     * @param args Various array of arguments to log.
     */
    info(...args: any[]): void {
        this.invoke('info', LoggingLevelType.INFO, ...args);
    }

    /**
     * Method wraps console.warn() function.
     *
     * @param args Various array of arguments to log.
     */
    warn(...args: any[]): void {
        this.invoke('warn', LoggingLevelType.WARNING, ...args);
    }

    /**
     * Method wraps console.error() function.
     *
     * @param args Various array of arguments to log.
     */
    error(...args: any[]): void {
        this.invoke('error', LoggingLevelType.ERROR, ...args);
    }
}
