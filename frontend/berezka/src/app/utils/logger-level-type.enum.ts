/**
 * author: akalashnikov
 * date: 21.04.2017.
 */
export enum LoggingLevelType {
    //noinspection JSUnusedGlobalSymbols
    NONE,
    ERROR,
    WARNING,
    INFO,
    DEBUG
}
