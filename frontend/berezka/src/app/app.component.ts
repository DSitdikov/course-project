import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';
import { User } from './entities/user';
import { Logger } from './utils/logger';
import { NotificationService } from './services/notification.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
    user: User;
    logger: Logger;
    busy: boolean;

    constructor(private authService: AuthService, private notificationService: NotificationService) {
        this.logger = new Logger({ prefix: 'app' });

        this.notificationService.busyChanged$
            .subscribe(busy => {
                this.logger.log('busy changed:', busy);
                this.busy = busy;
            });

        this.notificationService.userChanged$
            .subscribe(user => {
                this.user = user;
            });

        this.notificationService.setBusy(true);
    }

    ngOnInit() {
        this.authService.getUser()
            .then(user => {
                this.user = user;
                this.logger.log(user);
                this.notificationService.setBusy(false);
            })
            .catch(error => {
                this.logger.log(error);
                this.notificationService.setBusy(false);
            });
    }
}
