import { HttpModule } from '@angular/http';
import { async, inject, TestBed } from '@angular/core/testing';
import { ApiDataLoadingService } from './api.data-loading.service';

describe('WebApiClientDataLoadingService', () => {
    describe('with real backend', () => {
        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [HttpModule],
                providers: [ApiDataLoadingService]
            });

            it('should create service', inject(
                [ApiDataLoadingService],
                (service: ApiDataLoadingService) => {
                    expect(service).toBeTruthy();
                }
            ));
        });
    });
});
