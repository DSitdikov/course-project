import { ApiDataLoadingService } from './api.data-loading.service';
import { Logger } from '../utils/logger';
import { environment as env } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../entities/user';
import { NotificationService } from './notification.service';

@Injectable()
export class AuthService {
    private urls: any = env.urls;
    private logger: Logger;

    private logged: boolean;
    user: Promise<User>;

    constructor(private apiDataLoadingService: ApiDataLoadingService,
                private router: Router,
                private notificationService: NotificationService) {
        this.logger = new Logger({ prefix: 'auth service' });
    }

    isLogged(): boolean {
        return this.logged;
    }

    login(data: any): Promise<boolean> {
        return this.apiDataLoadingService.post(this.urls.login, { body: data })
            .then(response => {
                const key = response.key;
                this.setToken(key);
                this.logged = true;
                this.logger.log('token: ' + key);

                return true;
            })
            .catch(error => {
                throw error;
            });
    }

    logout(): Promise<boolean> {
        return this.apiDataLoadingService.post('auth/logout/')
            .then(data => {
                this.logged = false;
                this.user = undefined;
                this.removeToken();
                this.logger.log(data);

                return true;
            })
            .catch((error) => {
                throw error;
            });
    }

    getUser(): Promise<User> {
        if (!this.user) {
            this.user = this.apiDataLoadingService.get('auth/user/')
                .then(data => {
                    const user = new User(data);
                    this.logged = true;
                    this.logger.log(this.user);
                    return user;
                })
                .catch((error) => {
                    this.logged = false;
                    throw error;
                });
        }

        return this.user;
    }

    updateUser(user: User): any {
        this.apiDataLoadingService.put('auth/user', user)
            .then(() => {
                this.logger.log('success');
            })
            .catch((error) => {
                throw error;
            });
    }

    checkLogged(): boolean {
        this.notificationService.setBusy(true);

        if (!this.hasToken()) {
            return false;
        }

        this.apiDataLoadingService.get('auth/user/')
            .then((response) => {
                this.logger.log(response);
                this.logged = true;
                this.notificationService.setBusy(false);
                return true;
            })
            .catch((error) => {
                this.logger.log(error);
                this.notificationService.setBusy(false);
                return false;
            });
    }

    register(data: any): Promise<boolean> {
        return this.apiDataLoadingService.post('auth/registration/', { body: data })
            .then((response) => {
                this.logger.log(response);
                const key = response.key;
                this.setToken(key);

                this.logged = true;

                return true;
            })
            .catch((error) => {
                this.logger.log(error);
                throw error;
            });
    }

    // noinspection JSMethodCanBeStatic
    setToken(key: string): void {
        localStorage.setItem('token', key);
    }

    // noinspection JSMethodCanBeStatic
    getToken(): string {
        return localStorage.getItem('token');
    }

    // noinspection JSMethodCanBeStatic
    hasToken(): boolean {
        return !!localStorage.getItem('token');
    }

    // noinspection JSMethodCanBeStatic
    removeToken(): void {
        localStorage.removeItem('token');
    }
}
