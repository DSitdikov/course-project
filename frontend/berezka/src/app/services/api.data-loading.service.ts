import { Injectable } from '@angular/core';
import { environment as env } from '../../environments/environment';
import { Http, Request, RequestMethod, URLSearchParams, Headers, Response, RequestOptions } from '@angular/http';
import { Logger } from '../utils/logger';
import { Cookie } from 'ng2-cookies/ng2-cookies';

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/delay';

@Injectable()
export class ApiDataLoadingService {
    // Url to REST API
    private apiBaseUrl: string = env.webApiUrl;
    private logger: Logger;

    constructor(private http: Http) {
        this.logger = new Logger({
            prefix: 'data loader'
        });
    }

    get(operation: string, options?: object): Promise<any> {
        return this.invokeRequest(RequestMethod.Get, operation, options);
    }

    put(operation: string, options?: object): Promise<any> {
        return this.invokeRequest(RequestMethod.Put, operation, options);
    }

    post(operation: string, options?: object): Promise<any> {
        return this.invokeRequest(RequestMethod.Post, operation, options);
    }

    private invokeRequest(method: RequestMethod, operation: string, options?: any): Promise<any> {
        let preparedParams = null;

        options = options || {};

        if (options.params) {
            preparedParams = new URLSearchParams();
            for (const key in options.params) {
                if (options.params.hasOwnProperty(key)) {
                    preparedParams.set(key, options.params[key]);
                }
            }
        }

        const headers = new Headers();
        headers.append('Content-Type', 'application/json');

        const token = localStorage.getItem('token');
        if (token) {
            headers.append('Authorization', `Token ${token}`);
        }

        const csrf = Cookie.get('csrftoken');
        if (csrf) {
            headers.append('X-CSRFToken', csrf);
        }

        const request = new Request({
            method: method,
            url: this.apiBaseUrl + operation,
            params: preparedParams,
            body: JSON.stringify(options.body),
            headers: headers
        });

        return this.http.request(request)
            .delay(options.requestDelay || env.requestDelayDefault)
            .toPromise()
            .then((response: Response) => {
                // Some successful responses do not contain anything
                const data = 0 < response.text().trim().length ? response.json() : '';
                if (typeof data === 'object' && data._exception === true) {
                    // Web API error
                    this.logger.log(response);
                } else {
                    // Success
                    return data;
                }
            })
            .catch((error: Response) => {
                // We get error HTTP status, for example: 403, 404, 500
                throw error;
            });
    }
}
