import { Injectable } from '@angular/core';
import { Logger } from '../utils/logger';
import { Subject } from 'rxjs/Subject';
import { User } from '../entities/user';

@Injectable()
export class NotificationService {
    private logger: Logger;

    private busySource = new Subject<boolean>();
    private userChangedSource = new Subject<User>();

    busyChanged$ = this.busySource.asObservable();
    userChanged$ = this.userChangedSource.asObservable();

    constructor() {
        this.logger = new Logger({ prefix: 'notification service' });
    }

    // Use to show loading state and block the app (For example, loading json from server)
    setBusy(busy: boolean) {
        this.logger.log('set busy:', busy);
        this.busySource.next(busy);
    }

    setUser(user: User) {
        this.logger.log('user', user);
        this.userChangedSource.next(user);
    }
}
