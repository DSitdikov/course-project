import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Logger } from '../../utils/logger';
import { User } from '../../entities/user';
import { AuthService } from '../../services/auth.service';
import { NotificationService } from '../../services/notification.service';

@Component({
    selector: 'app-login',
    templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    logger: Logger;
    loginActive: boolean;
    registerActive: boolean;
    loginUser: any;
    registerUser: any;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private notificationService: NotificationService,
        private authService: AuthService) {
        this.logger = new Logger({ prefix: 'login component' });

        this.loginUser = {
            username: '',
            password: '',
            email: ''
        };

        this.registerUser = {
            username: '',
            password1: '',
            password2: '',
            email: ''
        };
    }

    ngOnInit() {
        if (this.authService.isLogged()) {
            this.router.navigate(['']);
        }

        this.route.fragment.subscribe((fragment: string) => {
            if (fragment === 'login') {
                this.setLoginActive();
            } else if (fragment === 'register') {
                this.setRegisterActive();
            }
        });
    }

    setLoginActive(): void {
        this.loginActive = true;
        this.registerActive = false;
    }

    setRegisterActive(): void {
        this.registerActive = true;
        this.loginActive = false;
    }

    login(user: User) {
        this.logger.log(user);
        this.authService.login(user)
            .then((response) => {
                this.logger.log(response);
                this.router.navigate(['']);
            })
            .catch((response) => {
                this.logger.log(response);
            });
    }

    register(user: User) {
        this.logger.log(user);

        this.registerUser.password2 = this.registerUser.password1;

        this.authService.register(user)
            .then(() => {
                this.router.navigate(['']);
            });
    }
}
