import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-contest',
    templateUrl: './contest.component.html'
})

export class ContestComponent {
    constructor(private route: ActivatedRoute) {
        let id = +this.route.snapshot.paramMap.get('id');

        console.log(id);
    }
}
