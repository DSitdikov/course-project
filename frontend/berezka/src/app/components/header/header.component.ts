import { Component, Input } from '@angular/core';
import { NotificationService } from '../../services/notification.service';
import { AuthService } from '../../services/auth.service';
import { User } from '../../entities/user';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html'
})

export class HeaderComponent {
    @Input()
    user: User;

    constructor(private notificationService: NotificationService,
                private router: Router,
                private authService: AuthService) {
    }

    logout(): void {
        this.authService.logout()
            .then(() => {
                this.router.navigate(['']);
            });
    }
}
