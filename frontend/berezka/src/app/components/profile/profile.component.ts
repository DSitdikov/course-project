import { Component } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Logger } from '../../utils/logger';
import { User } from '../../entities/user';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html'
})

export class ProfileComponent {
    logger: Logger;
    user: User;

    constructor(private authService: AuthService, private route: ActivatedRoute) {
        this.authService.getUser()
            .then((user) => {
                this.user = user;
            })
            .catch((error) => {
                this.logger.log(error);
            });
    }

    save(user: any): void {
        this.authService.updateUser(user)
            .then((response) => {
                this.logger.log(response);
            })
            .catch((error) => {
                this.logger.log(error);
            });
    }
}
