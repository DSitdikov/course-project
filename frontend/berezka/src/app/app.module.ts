import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ClarityModule } from 'clarity-angular';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ContestsComponent } from './components/contests/contests.component';
import { AboutComponent } from './components/about/about.component';
import { FaqComponent } from './components/faq/faq.component';
import { LoginComponent } from './components/login/login.component';
import { ContestComponent } from './components/contest/contest.component';

import { ApiDataLoadingService } from './services/api.data-loading.service';
import { NotificationService } from './services/notification.service';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard.service';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        ProfileComponent,
        ContestsComponent,
        AboutComponent,
        FaqComponent,
        LoginComponent,
        ContestComponent
    ],
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        ClarityModule.forRoot(),
        RouterModule.forRoot([
            {
                path: '',
                component: HomeComponent
            },
            {
                path: 'profile',
                component: ProfileComponent,
                canActivate: [AuthGuard],
            },
            {
                path: 'works',
                component: ContestsComponent
            },
            {
                path: 'works/:id',
                component: ContestComponent
            },
            {
                path: 'about',
                component: AboutComponent
            },
            {
                path: 'faq',
                component: FaqComponent
            },
            {
                path: 'login',
                component: LoginComponent
            }
        ])
    ],
    providers: [ApiDataLoadingService, AuthService, NotificationService, AuthGuard],
    bootstrap: [AppComponent]
})
export class AppModule {
}
