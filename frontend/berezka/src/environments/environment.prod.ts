export const environment = {
    production: true,
    logLevel: 'DEBUG',
    requestDelayDefault: 0,
    urls: {}
};
